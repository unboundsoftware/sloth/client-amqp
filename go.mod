module gitlab.com/unboundsoftware/sloth/client-amqp

go 1.21

require github.com/sparetimecoders/goamqp v0.3.1

require (
	github.com/google/uuid v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0 // indirect
)
