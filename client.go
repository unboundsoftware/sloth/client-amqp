/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package client

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/sparetimecoders/goamqp"
)

// Client keeps track of the AMQP-connection
type Client struct {
	serviceName string
	mapping     map[reflect.Type]string
	conn        *goamqp.Connection
	publisher   *goamqp.Publisher
}

// Publish publishes the provided request to be delivered at the time specified by when
func (c *Client) Publish(request interface{}, when time.Time) error {
	key, exists := c.mapping[reflect.TypeOf(request).Elem()]
	if !exists {
		return fmt.Errorf("no config found for type %s", reflect.TypeOf(request).String())
	}
	buff, err := json.Marshal(request)
	if err != nil {
		return fmt.Errorf("json marshal: %w", err)
	}
	return c.publisher.PublishWithContext(
		context.Background(),
		json.RawMessage(buff),
		goamqp.Header{
			Key:   "delay-until",
			Value: when,
		},
		goamqp.Header{
			Key:   "target",
			Value: fmt.Sprintf("amqp://%s/%s", c.serviceName, key),
		},
	)
}

// Close will close the AMQP-connection
func (c *Client) Close() error {
	return c.conn.Close()
}

// ResponseListener generates goamqp.ServiceResponseListener setups for the provided types
func (c *Client) ResponseListener(handler goamqp.HandlerFunc, types ...interface{}) goamqp.Setup {
	return func(conn *goamqp.Connection) error {
		for _, t := range types {
			eventType := reflect.TypeOf(t)
			key, exists := c.mapping[eventType]
			if !exists {
				return fmt.Errorf("missing routing key for type '%s'", eventType.String())
			}
			if err := goamqp.ServiceResponseConsumer("sloth", key, handler, t)(conn); err != nil {
				return err
			}
		}
		return nil
	}
}

// Mapping defines a mapping between a message type and a routing key
type Mapping struct {
	Type any
	Key  string
}

// New creates a new client, serviceName is the name of the service using the client and
// responseRoutingKey is the routing key which will be set on the responses from Sloth
func New(serviceName, amqpURL string, mappings ...Mapping) (*Client, error) {
	conn, err := goamqp.NewFromURL(serviceName, amqpURL)
	if err != nil {
		return nil, err
	}
	mapping := make(map[reflect.Type]string)
	publisher := goamqp.NewPublisher()
	setups := []goamqp.Setup{
		goamqp.ServicePublisher("sloth", publisher),
	}
	for _, r := range mappings {
		mapping[reflect.TypeOf(r.Type)] = r.Key
	}
	setups = append(setups, goamqp.WithTypeMapping("delay", json.RawMessage{}))
	err = conn.Start(context.Background(), setups...)
	if err != nil {
		return nil, err
	}
	return &Client{
		serviceName: serviceName,
		mapping:     mapping,
		conn:        conn,
		publisher:   publisher,
	}, nil
}
