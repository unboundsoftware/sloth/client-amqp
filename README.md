# AMQP Client library for sloth
```text
      `""==,,__
        `"==..__"=..__ _    _..-==""_
             .-,`"=/ /\ \""/_)==""``
            ( (    | | | \/ |
             \ '.  |  \;  \ /
              |  \ |   |   ||
         ,-._.'  |_|   |   ||
        .\_/\     -'   ;   Y
       |  `  |        /    |-.
       '. __/_    _.-'     /'
   jgs        `'-.._____.-'
```
A small client library for sending delayed requests to Sloth

## A simple example

```go
package main

import (
	"fmt"
	"gitlab.com/sparetimecoders/goamqp"
	"gitlab.com/unboundsoftware/sloth/client-amqp"
	"log"
	"os"
	"os/signal"
	"reflect"
	"syscall"
	"time"
)

func main() {
	c, err := client.New("service-a", "amqp://user:password@localhost:5672/", goamqp.Route{Type: SomeRequest{}, Key: "response"}, goamqp.Route{Type: OtherRequest{}, Key: "other-response"})
	if err != nil {
		panic(err)
	}
	conn, err := goamqp.NewFromURL("service-a", "amqp://user:password@localhost:5672/")
	if err != nil {
		panic(err)
	}
	handler := func(msg interface{}, headers goamqp.Headers) (response interface{}, err error) {
		switch response := msg.(type) {
		case *SomeRequest:
			fmt.Printf("Received some request: %+v\n", *response)
			// Handle response here
			return nil, nil
		case *OtherRequest:
			fmt.Printf("Received other request: %+v\n", *response)
			// Handle response here
			return nil, nil
		default:
			return nil, fmt.Errorf("unexpected type: %s", reflect.TypeOf(msg).String())
		}
	}
	err = conn.Start(
		goamqp.ServiceResponseListener("sloth", "response", handler, reflect.TypeOf(SomeRequest{})),
		goamqp.ServiceResponseListener("sloth", "other-response", handler, reflect.TypeOf(OtherRequest{})),
	)
	if err != nil {
		panic(err)
	}

	_ = c.Publish(&SomeRequest{"10 sec"}, time.Now().Add(10*time.Second))
	_ = c.Publish(&OtherRequest{"4 sec"}, time.Now().Add(4*time.Second))

	ic := make(chan os.Signal, 1)
	signal.Notify(ic, os.Interrupt, syscall.SIGTERM)

	<-ic
	log.Printf("signal received, exiting\n")
	_ = c.Close()
}

type SomeRequest struct {
	Message string
}

type OtherRequest struct {
	Message string
}
```
